package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	server := http.NewServeMux()
	server.HandleFunc("/", greet)

	log.Printf("Server listening on port %s", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), server); err != nil {
		log.Fatal(err)
	}
}

func greet(w http.ResponseWriter, r *http.Request) {
	log.Printf("Requested path: %s", r.URL.Path)
	greeting := "Hello, world!"
	host, _ := os.Hostname()
	ver := "0.1.0"
	ts := time.Now().UnixNano()
	json := fmt.Sprintf(`{"greet":"%s", "host":"%s", "version":"%s", "ts":"%d"}`, greeting, host, ver, ts)
	fmt.Fprintf(w, json)
}
